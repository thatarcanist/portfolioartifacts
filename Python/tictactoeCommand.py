import discord
import asyncio
import itertools

from commands.gamez.aux_functions import challange
from MythicalError import ErrorMessage
from discord.ext import commands

# This function checks the given index of the given grid to see if a player has placed a piece there
def checkSpace(index, grid):
    player1Piece = '⭕'
    player2Piece = '✖'
    if grid[index] == player1Piece or grid[index] == player2Piece:
        return False
    else:
        return True

class TicTacToe:

    def __init__(self, bot):
        self.bot = bot

    # This function checks the board for a winning pattern.
    # Due to each possibility being different, The checks are repetitive but necessary.
    async def checkBoard(self, grid):
        players = {
            "Player 1" : '⭕',
            "Player 2" : '✖'
        }

        for k, p in players.items():
            if grid[4] == p:  # Middle
                if grid[3] == p:  # Middle Across
                    if grid[5] == p:
                        winner = k
                        return winner
                if grid[1] == p:  # Middle Down
                    if grid[7] == p:
                        winner = k
                        return winner
                if grid[0] == p:  # Left to right diagonal down
                    if grid[8] == p:
                        winner = k
                        return winner
                if grid[2] == p:  # Right to left diagonal down
                    if grid[6] == p:
                        winner = k
                        return winner
            if grid[0] == p:  # Top left
                if grid[1] == p:  # Left Across
                    if grid[2] == p:
                        winner = k
                        return winner
                if grid[3] == p:  # Left Down
                    if grid[6] == p:
                        winner = k
                        return winner
            if grid[8] == p:  # Bottom Right
                if grid[7] == p:  # Right Across
                    if grid[6] == p:
                        winner = k
                        return winner
                if grid[5] == p:  # Left Down
                    if grid[2] == p:
                        winner = k
                        return winner

        #Lose Condition
        counter = 0
        for emoji in grid:
            player1Piece = '⭕'
            player2Piece = '✖'
            if emoji == player1Piece or emoji == player2Piece:
                counter += 1
        if counter == 9:
            winner = "No one"
            return winner

    # This function adds the player's piece to the board in the given location
    async def removePiece(self, game, emoji, player, index, playerpiece, grid):
        await game.remove_reaction(emoji=emoji, member=player)
        await game.remove_reaction(emoji=emoji, member=self.bot.user)
        grid.remove(emoji)
        grid.insert(index, playerpiece)
        return grid

    # This function displays the board in an Embedded message
    async def showBoard(self, player1, player2, player1Piece, player2Piece, grid, game, winner):
        counter = 0
        board = discord.Embed(title="Tic Tac Toe",
                              description=player1.mention + "(" + player1Piece + ") VS " + player2.mention + "(" + player2Piece + ")",
                              color=0x003366)

        viewGrid = ""
        # Loop to show the board in a 3x3 grid
        for spot in grid:
            counter += 1
            viewGrid += spot + " "
            if counter == 3:
                counter = 0
                viewGrid += "\n"
        board.add_field(name="Game", value=viewGrid, inline=True)
        if winner == "Player 1":
            board.add_field(name="Winner!", value=player1.mention, inline=True)
        if winner == "Player 2":
            board.add_field(name="Winner!", value=player2.mention, inline=True)
        if winner == "No one":
            board.add_field(name="Winner!", value="No one", inline=True)
        if winner is None:
            pass
        board.set_footer(text="React with the number of the spot you want to place your piece in!")
        await game.edit(embed=board)

    # Main code for the gae, Tic Tac Toe.
    @commands.command(pass_context=True, name="tictactoe", brief="Starts a game of tictactoe")
    async def tictac(self, ctx):

        # Using the challenge method to initiate a challenge to the second player
        accepted = await challange(ctx, self.bot, "tictactoe")
        if accepted == False:
            return
        else:
            pass

        # Sets the players and their pieces for the game
        player1 = ctx.message.author
        player1Piece = '⭕'
        player2 = ctx.message.mentions[0]
        player2Piece = '✖'

        # An enumerated dict with the numbered emojis for easier access.
        tileDict = {
            0: "1⃣",
            1: "2⃣",
            2: "3⃣",
            3: "4⃣",
            4: "5⃣",
            5: "6⃣",
            6: "7⃣",
            7: "8⃣",
            8: "9⃣"
        }

        # This creates the initial grid for the game which will be manipulated throughout the program.
        grid = []
        viewGrid = ""
        counter = 0
        for tile in tileDict.values():
            grid.append(tile)
            counter += 1
            viewGrid += tile + " "
            if counter == 3:
                counter = 0
                viewGrid += "\n"

        # Shows the board, After this point the method is used now that the grid is created.
        board = discord.Embed(title="Tic Tac Toe",
                              description=player1.mention +
                                          "(" + player1Piece + ") VS " +
                                          player2.mention + "(" + player2Piece +")",
                              color=0x003366)
        board.add_field(name="Game", value=viewGrid, inline=True)
        board.set_footer(text="React with the number of the spot you want to place your piece in!")

        # The message is set to a variable to allow easier manipulation.
        game = await ctx.channel.send(embed=board)

        # This loop creates all of the reactions to the game using the tileDict created earlier
        for tile in tileDict.values():
            await game.add_reaction(emoji=tile)

        # Using Itertools we use cycle to create a more compact loop when working through the players turns
        players = (
            player1,
            player2
            )
        cPlayers = itertools.cycle(iter(players))

        pieces = (
            player1Piece,
            player2Piece
        )
        cPieces = itertools.cycle(iter(pieces))

        # Loop begins to do each players turn.
        winner = False
        while winner is not True:
            playerPiece = (next(cPieces))
            playerP = (next(cPlayers))

            # Defines the second check of the script for wait_for
            def check2(reaction, user):
                return user == playerP and reaction.message.id == game.id

            # The turn will remain true until the user reacts with a tile that is not already taken
            turn = True
            while turn == True:
                res = await self.bot.wait_for('reaction_add', check=check2)
                res = (res[0].emoji)

                # Loops through the dict, k being the index and p being the emoji
                for k, p in tileDict.items():
                    if res == p:
                        free = (checkSpace(k, grid))
                        if free is True:
                            grid = await TicTacToe.removePiece(self, game, p, playerP, k, playerPiece, grid)
                            turn = False
                            break

            # Using the checkBoard function, If the board has a winning pattern, the game ends and the loop is broken.
            if await TicTacToe.checkBoard(self, grid) is None:
                await TicTacToe.showBoard(self, player1, player2, player1Piece, player2Piece, grid, game, winner)
                pass
            else:
                winner = await TicTacToe.checkBoard(self, grid)
                await TicTacToe.showBoard(self, player1, player2, player1Piece, player2Piece, grid, game, winner)
                break

def setup(bot):
    bot.add_cog(TicTacToe(bot))