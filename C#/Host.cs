﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

public class UDPListener
{
    private const int listenPort = 11000;

    private static void StartListener()
    {
        UdpClient listener = new UdpClient(listenPort);
        IPEndPoint groupEP = new IPEndPoint(IPAddress.Broadcast, listenPort);

        try
        {

            bool gotit = false;
            // This will loop until we get the right message
            while (!gotit)
            {
                Console.WriteLine("Waiting for broadcast");
                byte[] bytes = listener.Receive(ref groupEP);

                Console.WriteLine($"Received broadcast from {groupEP} :");
                Console.WriteLine($" {Encoding.ASCII.GetString(bytes, 0, bytes.Length)}");

                // This checks if we got the correct message
                if (Encoding.ASCII.GetString(bytes, 0, bytes.Length) == "Hi")
                {
                    // This will create a socket with the person who started talking to us

                    Socket s = new Socket(groupEP.AddressFamily, SocketType.Dgram, ProtocolType.Udp);

                    //This will send back the same message
                    byte[] sendbuf = Encoding.ASCII.GetBytes(Encoding.ASCII.GetString(bytes, 0, bytes.Length));
                    IPEndPoint ep = new IPEndPoint(groupEP.Address, 11000);
                    Thread.Sleep(1000);
                    s.SendTo(sendbuf, ep);
                }

            }
            Console.WriteLine("I'm freee!");
        }
        catch (SocketException e)
        {
            Console.WriteLine(e);
        }
        finally
        {
            listener.Close();
        }
    }

    public static void Main()
    {
        StartListener();
    }
}