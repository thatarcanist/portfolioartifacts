import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Logger {

    private static final Logger INSTANCE = new Logger(false);

    private static File logFile = new File("log_" +
            Calendar.getInstance().getTime().toString().replace(" ", "-").replace(":", "_") + ".csv");
    private static List<String> fileQueue = new ArrayList<>();

    private static boolean perfMode;

    private static boolean haveDateStamp = true;

    public Logger(boolean perfMode){
        Logger.perfMode = perfMode;
    }

    public static void log(Priority priority, String message){

        String logOutput;
        String consoleOutput;

        if (haveDateStamp){
            logOutput = Calendar.getInstance().getTime() + "," + priority.name() + "," + message;
            consoleOutput = "[" + Calendar.getInstance().getTime() + "] " + priority.name() + ": " + message;
        }
        else{
            logOutput = priority.name() + "," + message;
            consoleOutput = priority.name() + ": " + message;
        }

        System.out.println(consoleOutput);

        if (perfMode){
            fileQueue.add(logOutput);
        } else {
            try{

                BufferedWriter br = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(logFile, true)));
                br.write(logOutput);
                br.newLine();
                br.close();

            }
            catch (IOException ioe){
                Logger.log(Priority.SEVERE, "Logger IO Exception");
            }

        }

    }

    public static void flush(){
        if (perfMode){
            try {
                FileWriter fileWriter = new FileWriter(logFile);
                for (String str : fileQueue){
                    fileWriter.write(str + "\n");
                }
                fileWriter.close();
            } catch (IOException ioe) {
                Logger.log(Priority.SEVERE, "Logger IO Exception");
            }
        } else {
            Logger.log(Priority.LOW, "No need to flush, Performance mode is off.");
        }



    }

    public static Logger getInstance(){
        return INSTANCE;
    }

    public static void setPerfMode(boolean perfMode){
        Logger.perfMode = perfMode;
    }

    public static Boolean getPerfmode(){
        return Logger.perfMode;
    }

}
