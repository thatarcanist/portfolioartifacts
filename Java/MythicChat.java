package net.zachariah.mythic.core.chat;

import net.zachariah.mythic.core.MythicMain;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.regex.PatternSyntaxException;

public class MythicChat {

    private final MythicMain plugin;
    private final File channelFile;
    private YamlConfiguration channelConfig;
    private String id;

    public MythicChat(MythicMain plugin, String id){
        this.plugin = plugin;
        this.id = id;
        File channelsDirectory = new File(plugin.getDataFolder(), "channels");
        if (!channelsDirectory.exists()) {
            channelsDirectory.mkdirs();
        }

        this.channelFile = new File(channelsDirectory, id + ".yml");
        this.channelConfig = YamlConfiguration.loadConfiguration(channelFile);
        if (!channelFile.exists()){
            try {
                channelFile.createNewFile();
                channelConfig.set("radius", 5);
                channelConfig.set("id", id);
                channelConfig.set("format", "[{channel}] {name}/{nick} says: {msg}");
                channelConfig.set("conditional.separator", null);
                channelConfig.set("conditional.format", null);
                save();
            }
            catch (IOException io){
                io.printStackTrace();
            }
        }
    }

    public void send(Player player, String message){
        if (player.hasPermission("mythic.core." + getID() + ".send")){
            if (getRadius() < 0){
                GlobalSend(player, message);
            }
            else{
                ProximitySend(player, message);
            }
        }
        else {
            player.sendMessage(ChatColor.RED + "You can't send messages in this channel.");
        }
    }

    private void ProximitySend(Player player, String message){
        player.getWorld().getPlayers()
                .stream()
                .filter(otherPlayer -> otherPlayer.getLocation().distanceSquared(player.getLocation()) <= this.getRadius() * this.getRadius())
                .forEach(otherPlayer -> otherPlayer.sendMessage(parseFormat(player, message))
                );
    }

    private void GlobalSend(Player player, String message){
        for (Player player2 : plugin.getServer().getOnlinePlayers()){
            if (player2.hasPermission("mythic.core." + getID() + ".read")){
                player2.sendMessage(parseFormat(player, message));
            }
        }
    }


    public String parseFormat(Player player, String message){
        String format = getFormat();
        String name = player.getName();
        String nick = player.getDisplayName();

        format = format.replace("{name}", name);
        format = format.replace("{nick}", nick);
        format = format.replace("{channel}", getID());

        format = format.replace("{msg}", message);
        if (player.hasPermission("mythic.core.ChatFormat")){
            format = ChatColor.translateAlternateColorCodes('&', format);
        }


        if (getConditionalSeparator() != null && message.contains(getConditionalSeparator())) {
            // Split the mesasge
            String[] messageSplit;
            try{
                messageSplit = format.split(getConditionalSeparator());
            } catch (PatternSyntaxException e){
                messageSplit = format.split("\\*");
            }

            String temp = "";
            for (int i = 0; i < messageSplit.length; i++){
                if (i % 2 != 0){
                    // Message[i] is the portion that we want to reformat
                    messageSplit[i] = getConditionalSeparator() + messageSplit[i] + getConditionalSeparator();
                    // This is the format we want to use
                    String conditionalFormat = getConditionalFormat();
                    // Message[i] gets put in place in the conditional formatter
                    messageSplit[i] = conditionalFormat.replace("{msg}", messageSplit[i]);
                    // Message[i] gets put into color
                    if (player.hasPermission("mythic.core.ChatFormat")){
                        messageSplit[i] = ChatColor.translateAlternateColorCodes('&', messageSplit[i]);
                    }
                    try{
                        messageSplit[i+1] = ChatColor.getLastColors(messageSplit[i-1]) + messageSplit[i+1];
                    } catch (ArrayIndexOutOfBoundsException e){
                        // Pass
                    }
                }
            }
            for (String string : messageSplit){
                temp += string;
            }
            format = temp;
        }



        return format;


    }

    public int getRadius() {
        return channelConfig.getInt("radius");
    }

    public String getFormat() {
        return channelConfig.getString("format");
    }

    public String getID() {
        return channelConfig.getString("id");
    }

    public String getAlias() {
        return channelConfig.getString("alias");
    }

    public String getConditionalSeparator(){
        return channelConfig.getString("conditional.separator");
    }

    public String getConditionalFormat(){
        return channelConfig.getString("conditional.format");
    }

    private void save() {
        try {
            channelConfig.save(channelFile);
        } catch (IOException exception) {
            plugin.getLogger().log(Level.SEVERE, "Failed to save channel file", exception);
        }
    }


}
