package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Random;

@Controller
public class WebController implements WebMvcConfigurer {

    private Random rand = new Random();
    private int num = rand.nextInt(100);

    //@Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/correct").setViewName("correct");
    }

    @GetMapping("/")
    public String showForm(GuessForm guessForm, Model model, HttpSession session) {

        Integer Attempts = (Integer) session.getAttribute("attempts");
        if (Attempts == null) {
            Attempts = new Integer(0);
            session.setAttribute("attempts", Attempts);
        }

        model.addAttribute("attempts", Attempts);

        return "form";
    }

    @PostMapping("/")
    public String checkGuessInfo(@Valid GuessForm guessForm, Model model, HttpSession session, BindingResult bindingResult) {

        Integer Attempts = (Integer) session.getAttribute("attempts");
        if (Attempts == null) {
            Attempts = new Integer(0);
            session.setAttribute("attempts", Attempts);
        }

        if (bindingResult.hasErrors()) {
            Attempts++;
            session.setAttribute("attempts", Attempts);
            model.addAttribute("attempts", Attempts);
            return "form";
        }

        if (guessForm.getGuess() == num){
            session.setAttribute("attempts", 0);
            model.addAttribute("attempts", Attempts);
            return "correct";
        } else{
            Attempts++;
            session.setAttribute("attempts", Attempts);
            model.addAttribute("attempts", Attempts);
            return "form";
        }


    }


    @RequestMapping("/cheat")
    public String cheat(Model model, HttpSession session) {

        model.addAttribute("answer", num);

        return "cheat";

    }

    @RequestMapping("/reset")
    public String reset(Model model, HttpSession session) {

        Integer SessionInt = (Integer) session.getAttribute("attempts");
        if (SessionInt == null) {
            SessionInt = new Integer(0);
            session.setAttribute("attempts", SessionInt);
        }

        model.addAttribute("attempts", SessionInt);
        SessionInt++;
        session.setAttribute("attempts", 0);

        num = rand.nextInt(100);

        return "reset";

    }
}